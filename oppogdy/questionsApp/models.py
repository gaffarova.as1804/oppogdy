from django.db import models

class Question(models.Model):
	title = models.CharField(max_length=500)
	createdQuestion = models.DateTimeField(auto_now_add=True)
	owner = models.ForeignKey('auth.User', related_name='questions', on_delete=models.SET_NULL, null = True)
	public = models.BooleanField(default=False)
	answer = models.TextField(null=True)
	createdAnswer = models.DateTimeField(null = True)

	class Meta:
		ordering = ['createdQuestion']


class Comment(models.Model):
	body = models.TextField(blank=False)
	created = models.DateTimeField(auto_now_add=True)
	owner = models.ForeignKey('auth.User', related_name='comments', on_delete=models.CASCADE)
	question = models.ForeignKey('Question', related_name='comments', on_delete=models.CASCADE)

	class Meta:
		ordering=['created']
