from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
	path('users/', views.UserList.as_view()),
	path('users/<int:pk>/', views.UserDetail.as_view()),
	path('questions/', views.QuestionList.as_view()),
	path('questions/<int:pk>/', views.QuestionDetail.as_view()),
	path('comments/', views.CommentList.as_view()),
	path('comments/<int:pk>/', views.CommentDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)