from rest_framework import serializers
from django.contrib.auth.models import User
from .models import Question, Comment

class PersonSerializer(serializers.ModelSerializer):
	class Meta:
		model=User
		fields=['first_name', 'last_name']

	def to_representation(self,instance):
		result = super(PersonSerializer, self).to_representation(instance)
		new_result = '{} {}'.format(result['first_name'], result['last_name'])
		return new_result

class QuestionListSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Question
		fields = ['id', 'title', 'answer']

class CommentSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Comment
        fields = ['id', 'body', 'owner', 'created', 'question']

class QuestionDetailSerializer(serializers.ModelSerializer):
	owner = PersonSerializer()
	comments = CommentSerializer(many=True, read_only=True)

	class Meta:
		model = Question
		fields = ['id', 'title', 'createdQuestion', 'owner', 'answer', 'createdAnswer', 'comments']
		depth = 1


class UserSerializer(serializers.ModelSerializer):
	questions = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
	comments = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

	class Meta:
		model=User
		fields=['id', 'username', 'first_name', 'last_name']



# Вывести часто задаваемые запросы (парметр паблик у вопроса и есть ответ)
# Вывести вопросы конкретного пользователя (брать данные о пользователе из хедера)
# Обращаться не по id к конкретному вопросу
# Создание нового вопроса
# Авторизация

# Администратор делает вопрос публичным

