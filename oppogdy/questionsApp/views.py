from rest_framework import generics, permissions
from django.views.decorators.csrf import csrf_exempt
from . import serializers
from django.contrib.auth.models import User
from .models import Question, Comment
from .serializers import QuestionListSerializer, QuestionDetailSerializer
from .permissions import IsOwnerOrReadOnly

class UserList(generics.ListAPIView):
	queryset = User.objects.all()
	serializer_class = serializers.UserSerializer

class UserDetail(generics.RetrieveAPIView):
	queryset = User.objects.all()
	serializer_class = serializers.UserSerializer

class QuestionList(generics.ListCreateAPIView):
	queryset = Question.objects.all()
	serializer_class = serializers.QuestionListSerializer
	permission_classes = [permissions.IsAuthenticatedOrReadOnly]

	def perform_create(self, serializer):
		serializer.save(owner=self.request.user)

class QuestionDetail(generics.RetrieveAPIView):
	context_object_name = 'question'
	queryset = Question.objects.all()
	serializer_class = serializers.QuestionDetailSerializer

	def get_context_data(self, **kwargs):
		context = super(QuestionDetail,  self).get_context_data(**kwargs)
		context['comment_list'] = Comment.objects.all()
		return context

class CommentList(generics.ListCreateAPIView):
	context_object_name = 'comment_list'
	queryset = Comment.objects.all()
	serializer_class = serializers.CommentSerializer
	permission_classes = [permissions.IsAuthenticatedOrReadOnly]

	def perform_create(self, serializer):
		serializer.save(owner=self.request.user)

class CommentDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Comment.objects.all()
    serializer_class = serializers.CommentSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly,
                          IsOwnerOrReadOnly]



